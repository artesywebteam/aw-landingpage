<?php

// Archivo de configuracion del archivo de mail.php

$asunto="Formulario de contacto";

// correo de autoresponder en caso que el lead no ponga email
$correo_remitente="noresponderartesyweb@gmail.com";

// correos de los clientes a enviar los leads
$correos_clientes=array(
    "cliente@cliente.com",
    // para agregar mas correos, simplemente copiar la linea de arriba, presionar la tecla enter y pegar
);

// correos copias para enviar para tener registro del lead
$correos_copias=array(
    "soporte@artesyweb.com",
    // para agregar mas correos, simplemente copiar la linea de arriba, presionar la tecla enter y pegar
);

// correos copias para enviar en caso de un error en el registro del lead en el  sistema
$correos_error=array(
    "erroresdelsistema.aw@gmail.com",
    "pabloandi@gmail.com",
    "delavegadiego@gmail.com",

    // para agregar mas correos, simplemente copiar la linea de arriba, presionar la tecla enter y pegar
);


 ?>
