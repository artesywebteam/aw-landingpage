$.fn.validator.Constructor.INPUT_SELECTOR = ':input:not([type="submit"], button):enabled:visible, .selectpicker';

jQuery().ready(function(){
		$('#enviar_email').validator().on('submit', function (e) {
			//e.preventDefault();
			if (e.isDefaultPrevented()) {
				// handle the invalid form...
				return false;
			} else {
				//send notifier
				var form_data = jQuery("#enviar_email").serialize();
				var error = "";

				jQuery.ajax({
				    type: 'POST',
				    //url: 'http://127.0.0.1:8080/subscribe/formlead',
				    url: 'https://artesyweb.xyz/subscribe/formlead',
				    crossDomain: true,
				    data: form_data+"&url="+window.location.origin+window.location.pathname,
				    dataType: 'json',
					beforeSend: function(jqXHR,settings){
						jQuery('#boton-enviar').button('loading');
					},
				    success: function(responseData, textStatus, jqXHR) {
				        console.log('POST OK.');
				    },
				    error: function (responseData, textStatus, errorThrown) {
				        console.log('POST failed.');
						error="&error="+errorThrown+"&url="+window.location.href;
						jQuery('#boton-enviar').button("reset");
				    },
				    complete: function ( jqXHR,  textStatus ){
						jQuery.ajax({
						    type: 'POST',
						    url: 'lib/mail.php',
						    data: form_data+error,
							beforeSend: function(jqXHR,settings){
								jQuery('#boton-enviar').button('loading');
							},
						    success: function(responseData, textStatus, jqXHR) {
						        console.log('POST OK.');
								window.location.replace("gracias.html");
							},
						    error: function (responseData, textStatus, errorThrown) {
						        console.log('POST failed.');
								jQuery("#respuesta").html("Ha ocurrido un error. Por favor intentelo de nuevo");
								jQuery('#boton-enviar').button("reset");
						    },
						    complete: function ( jqXHR,  textStatus ){
								e.preventDefault();
						    }
						});
				    }
				});

				return false;
			}

		});

});
